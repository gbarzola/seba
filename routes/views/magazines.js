var keystone = require('keystone'),
	moment = require('moment');

var Magazine = keystone.list('Magazine');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.section = 'revistas';
	
	view.query('magazines',
		Magazine.model.find()
			.sort('-date'));

	view.render('site/magazines');
	
}
