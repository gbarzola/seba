var keystone = require('keystone'),
	moment = require('moment');

var Links = keystone.list('Link');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.section = 'links';
	
	view.query('links',
		Links.model.find());
	
	view.render('site/links');
	
}
