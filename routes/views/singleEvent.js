var keystone = require('keystone'),
	moment = require('moment');

var Event = keystone.list('Event');

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.section = 'eventos';
	
	view.on('init', function(next) {
		Event.model.findOne()
			.where('slug', req.params.event)
			.exec(function(err, single) {
				if (err) return res.err(err);
				if (!single) return res.notfound('Evento no encontrado');				
				locals.event = single;
				next();
			});
	});
	
	view.render('site/event');
	
}
