var keystone = require('keystone'),
	moment = require('moment');

var Event = keystone.list('Event');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.section = 'eventos';
	
	view.query('events.upcoming',
		Event.model.find()
			.where('date').gte(moment().startOf('day').toDate())
			.sort('-date'));
	
	view.query('events.past',
		Event.model.find()
			.where('date').lt(moment().subtract('days', 1).endOf('day').toDate())
			.sort('-date'));
	
	view.render('site/meetups');
	
}
