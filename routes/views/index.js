var keystone = require('keystone'),
moment = require('moment');

var Event = keystone.list('Event'),
Post = keystone.list('Post');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
	locals = res.locals;

	locals.section = 'home';
	
	// Load the next meetup
	view.query('nextMeetup',
		Event.model.findOne()
		.where('date').gte(moment().startOf('day').toDate())
		.sort('date'));
	
	// Load the last meetup
	view.query('lastMeetup',
		Event.model.findOne()
		.where('date').lt(moment().startOf('day').toDate())
		.sort('-date'));
	
	// Load recent posts
	view.query('posts',
		Post.model.find()
		.where('state', 'published')
		.sort('-publishedDate')
		.limit(5)
		.populate('author categories'));
	
	view.render('site/index');
	
}
