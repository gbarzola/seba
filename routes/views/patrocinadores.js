var keystone = require('keystone'),
	moment = require('moment');

var Sponsor = keystone.list('Sponsor');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	locals.section = 'patrocinadores';
	
	view.query('sponsors',
		Sponsor.model.find());

	view.render('site/patrocinadores');
	
}
