var keystone = require('keystone'),

exports = module.exports = function(req, res) {	
	var view = new keystone.View(req, res),
	locals = res.locals;

	// Init locals
	locals.section = req.params.page;
	locals.data = {};
	// Load the current category filter
	view.on('init', function(next) {
		keystone.list('Page').model.findOne({ 'slug': req.params.page }).exec(function(err, result) {
			locals.data.page = result;
			next(err);
		});
	});
	// Render the view
	view.render('page');
}
