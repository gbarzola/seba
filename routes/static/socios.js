var keystone = require('keystone'),

exports = module.exports = function(req, res) {	
	var pageName = "socios"
	var view = new keystone.View(req, res),
	locals = res.locals;	

	// Init locals
	locals.section = pageName;
	locals.data = {};
	// Load the current category filter
	view.on('init', function(next) {
		keystone.list('Page').model.findOne({ 'slug': pageName }).exec(function(err, result) {
			locals.data.page = result;
			next(err);
		});
	});
		
	// Render the view
	view.render('page');
	
}
