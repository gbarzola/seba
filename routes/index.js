var _ = require('underscore'),
	keystone = require('keystone'),
	middleware = require('./middleware'),
	importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initErrorHandlers);
keystone.pre('routes', middleware.initLocals);
keystone.pre('routes', middleware.initSlider);
keystone.pre('render', middleware.flashMessages);

// Handle 404 errors
keystone.set('404', function(req, res, next) {
	res.notfound();
});

// Handle other errors
keystone.set('500', function(err, req, res, next) {
	var title, message;
	if (err instanceof Error) {
		message = err.message;
		err = err.stack;
	}
	res.err(err, title, message);
});

// Load Routes
var routes = {
	api: importRoutes('./api'),
	views: importRoutes('./views'),
	static: importRoutes('./static'),
	authentication: importRoutes('./authentication')
};

// Bind Routes
exports = module.exports = function(app) {
	
	// Old
	// app.get('/', routes.views.old);
	
	// Website
	app.get('/', routes.views.index);
	app.get('/eventos', routes.views.events);
	app.get('/eventos/:event', routes.views.singleEvent);
	app.get('/revistas', routes.views.magazines);
	app.get('/patrocinadores', routes.views.patrocinadores);
	app.get('/links', routes.views.links);
	app.get('/blog/:category?', routes.views.blog);
	app.all('/blog/post/:post', routes.views.post);
	app.get('/institucional',routes.static.institucional);
	app.get('/socios',routes.static.socios);


	// Session
	app.all('/:mode(signin|join|attend)', routes.views.signin);
	app.get('/signout', routes.views.signout);


}
