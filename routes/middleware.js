var _ = require('underscore'),
	querystring = require('querystring'),
	keystone = require('keystone');


/**
	Initialises the standard view locals
*/

exports.initLocals = function(req, res, next) {
	
	var locals = res.locals;
	
	locals.navLinks = [
		{ label: 'Institucional',      key: 'institucional',      href: '/institucional',          layout: 'left' },
		{ label: 'Socios',   key: 'socios',   href: '/socios',   layout: 'left' },
		{ label: 'Eventos',     key: 'eventos',     href: '/eventos',     layout: 'left' },
		{ label: 'Revistas',   key: 'revistas',   href: '/revistas',   layout: 'left' },
		//{ label: 'Videos',     key: 'videos',     href: '/videos',     layout: 'right' },
		{ label: 'Patrocinadores', key: 'patrocinadores', href: '/patrocinadores', layout: 'right' },
		{ label: 'Links', key: 'links', href: '/links', layout: 'right' },
		{ label: 'Inicio',     key: 'home',     href: '/',     layout: 'right' }
	];
	
	locals.user = req.user;
	locals.qs_set = qs_set(req, res);

	keystone.list('Sponsor').model.find().exec(function(err, results) {
		if (err || !results.length) {
			return next(err);
		}
		locals.sponsors = results;
		next(err);
	});
	
};

exports.initSlider = function(req, res, next) {
	
	var locals = res.locals;

	keystone.list('Slider').model.find().exec(function(err, results) {
		if (err || !results.length) {
			return next(err);
		}
		locals.slider = results[0];
		next(err);
	});
};


/**
	Inits the error handler functions into `req`
*/

exports.initErrorHandlers = function(req, res, next) {
	
	res.err = function(err, title, message) {
		res.status(500).render('errors/500', {
			err: err,
			errorTitle: title,
			errorMsg: message
		});
	}
	
	res.notfound = function(title, message) {
		res.status(404).render('errors/404', {
			errorTitle: title,
			errorMsg: message
		});
	}
	
	next();
	
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {
	
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};
	
	res.locals.messages = _.any(flashMessages, function(msgs) { return msgs.length }) ? flashMessages : false;
	
	next();
	
};

/**
	Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
	
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/signin');
	} else {
		next();
	}
	
}


/**
	Returns a closure that can be used within views to change a parameter in the query string
	while preserving the rest.
*/

var qs_set = exports.qs_set = function(req, res) {

	return function qs_set(obj) {

		var params = _.clone(req.query);

		for (var i in obj) {
			if (obj[i] === undefined || obj[i] === null) {
				delete params[i];
			} else if (obj.hasOwnProperty(i)) {
				params[i] = obj[i];
			}
		}

		var qs = querystring.stringify(params);

		return req.path + (qs ? '?' + qs : '');

	}

}
