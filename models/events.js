var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Event = new keystone.List('Event', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Event.add({
	title: { type: String, required: true },
	slug: { type: String, index: true },
	content: {type: Types.Html, wysiwyg: true, height: 400},
	location:{ type: Types.Location },
	cover:{type:Types.CloudinaryImage},
	date: { type: Types.Date, required: true, initial: true, index: true },
});

Event.defaultColumns = 'title, slug|20%, location|20%';
Event.register();
