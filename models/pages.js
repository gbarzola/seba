var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Page = new keystone.List('Page', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Page.add({
	title: { type: String, required: true },
	slug: { type: String, index: true },
	content: {type: Types.Html, wysiwyg: true, height: 400},
	file: { type:Types.AzureFile,containerFormatter:function(item,filename){return "aave";}},
});

Page.defaultColumns = 'title, slug|20%';
Page.register();
