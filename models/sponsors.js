var keystone = require('keystone'), Types = keystone.Field.Types;

var Sponsor = new keystone.List('Sponsor', {
	map: { name: 'title' },
	autokey: { path: 'title', from: 'title', unique: true }
});

Sponsor.add({
	title: { type: String, required: true, index:true },
	name: { type: String},
	url:{type:Types.Url},
	logo:{type:Types.CloudinaryImage},
	email:{type:Types.Email},
	alwaysVisible:{type:Types.Boolean},
});

Sponsor.defaultColumns = 'title, email|20%, url|20%, alwaysVisible:10%';
Sponsor.register();
