var keystone = require('keystone'), Types = keystone.Field.Types;

var slider = new keystone.List('Slider', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

slider.add({
	title: { type: String, required: true },
	pictures:{ type: Types.CloudinaryImages }
});

slider.defaultColumns = 'title';
slider.register();
