var keystone = require('keystone'), Types = keystone.Field.Types;

var Link = new keystone.List('Link', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Link.add({
	title: { type: String, required: true },
	slug: { type: String, index: true },
	url: {type:Types.Url}
});

Link.defaultColumns = 'title, slug|20%, url|50%';
Link.register();
