var keystone = require('keystone'), Types = keystone.Field.Types;

var Magazine = new keystone.List('Magazine', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Magazine.add({
	title: { type: String, required: true },
	slug: { type: String, index: true },
	cover:{ type:Types.CloudinaryImage},
	file: { type:Types.AzureFile,containerFormatter:function(item,filename){return "aave";}},
	date: { type: Types.Date},
	content: {type: Types.Html, wysiwyg: true, height: 400},
});

Magazine.defaultColumns = 'title, slug|20%';
Magazine.register();
