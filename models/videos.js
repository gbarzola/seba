var keystone = require('keystone'), Types = keystone.Field.Types;

var Video = new keystone.List('Video', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Video.add({
	title: { type: String, required: true },
	slug: { type: String, index: true },
	cover:{ type:Types.CloudinaryImage},
	file: { type:Types.AzureFile,containerFormatter:function(item,filename){return "aave";}},
	content: {type: Types.Html, wysiwyg: true, height: 400},
	recordedOn:{type:Types.Datetime}
});

Video.defaultColumns = 'title, slug|20%';
Video.register();
