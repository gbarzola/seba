jQuery.noConflict();
jQuery(document).ready(function($){

	//Menu
	this.navLi = $('#main_menu li').children('ul').end();
	//Add arrow for each menu item that has a children
	$.each(this.navLi, function() {
		if ( $(this).children('li ul')[0] ) {
			$(this).children('a').append(' <span class="menu_plus">+</span> ');
		}
	});
	//keep marked parrent element
	this.navLi
		.mouseenter(function() {
			$(this).children('a:first').addClass("hov");
		})
		.mouseleave(function() {
			$(this).children('a:first').removeClass("hov");
		} );

	//selectnav('main_menu');


	$("#homepage_slider1" ).smkSlider({
		firstSlide: 1,
		autoplay: true,
		interval: 12000,
		progressBar: true,
		showBullets: true,
		responsiveScale: true,
		width: 800,
		height: 300,
	});
	
	/* Contact form */
	$('#contact_form').submit(function(e){

		e.preventDefault();

		var action = $(this).attr('action');
		var this_form = $(this);
		
		$('#contact_form .form_message').html('<h2>Please wait!</h2>').removeClass('form_error form_success').addClass('form_loading').show();

		if( $('#contact_form .loader').length > 0 ){
			$('#contact_form .loader').fadeIn('fast');
			$('#contact_form #submit').attr('disabled', true);
		}
		else{
			$('#contact_form .form_message').prepend('<div class="loader"></div>');
			$('#contact_form #submit').attr('disabled', true);
		}
		
		$('#contact_form input, #contact_form textarea').each(function(){
			contact_form_remove_msg(this);
		});
		
		var all_fields =  $(this).serialize();
		$.ajax({
			type: 'POST',
			url: action,
			data: all_fields,
			dataType: 'json',
			success: function(result) {
				if(result['status'] == 'fail'){
					contact_form_error(result, 'name');
					contact_form_error(result, 'email');
					contact_form_error(result, 'message');
					contact_form_error(result, 'verify');
					$('#contact_form .loader').fadeOut('fast');
					$('#contact_form #submit').attr('disabled', false);
					$('#contact_form .form_message')
						.html('<h2>Attention! Please correct the errors below and try again.</h2>')
						.removeClass('form_loading form_success')
						.addClass('form_error');
				}
				else if(result['status'] == 'success'){
					$('#contact_form .loader').fadeOut('fast');
					$('#contact_form #submit').attr('disabled', false);
					$('#contact_form .form_message')
						.html('<h2>Email Sent Successfully.</h2>Thank you <strong>' + $('input[name=name]').val() + '</strong>, your message has been submitted to us.')
						.removeClass('form_error form_loading')
						.addClass('form_success');
				}
				else{
					$('#contact_form .loader').fadeOut('fast');
					$('#contact_form #submit').attr('disabled', false);
				}
			}
		});


		return false;
		
		function contact_form_error(_result, _name){
			if(_result[_name]){
				this_form.find('input[name=' + _name + '], textarea[name=' + _name + ']')
					.after('<div class="contact_error_' + _name + '">' + _result[_name] + '</div>')
					.addClass('contact_field_error');

				this_form.find('.contact_error_' + _name).addClass('contact_error_message');
			}
		}

	});

	$('#contact_form input, #contact_form textarea').each(function(){
		$(this).on('click', function(){
			contact_form_remove_msg(this);
		});
		$(this).parent('div').css({'position': 'relative'});
	});

	function contact_form_remove_msg(obj){
		var t = $(obj);
		var _name = $(obj).attr('name');
		t.removeClass('contact_field_error').next('.contact_error_' + _name).remove();
	}
});