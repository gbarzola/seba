// Load .env for development environments
require('dotenv').load();

// Initialise New Relic if an app name and license key exists
if (process.env.NEW_RELIC_APP_NAME && process.env.NEW_RELIC_LICENSE_KEY) {
	require('newrelic');
}

/**
 * Application Initialisation
 */

var keystone = require('keystone'),
	pkg = require('./package.json');

keystone.init({

	'name': 'Asociacion Argentina de Veterinaria Equina',
	'brand': 'Asociacion Argentina de Veterinaria Equina',

	'favicon': 'public/favicon.ico',
	'less': 'public',
	'static': 'public',

	'views': 'templates/views',
	'view engine': 'jade',
	
	'emails': 'templates/emails',

	'auto update': true,
	'mongo': process.env.MONGOLAB_URI ||  process.env.MONGOHQ_URL || process.env.MONGO_URI || 'mongodb://localhost/'+ pkg.name,

	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': ')r=AzxJ%&4z3QC|,*)3F=2msEgmJc;!.<(4/vTNydos(Rb!-]m%gS5M}c4mcKA*r',
	'google api key': process.env.GOOGLE_BROWSER_KEY	
});

keystone.set('default region', 'AR');
keystone.set('azurefile config', { account: 'beto', key: 'OSg1BPl4/8Y7RoYdQ3wSZRODpjosgFb5C11D8bSjxf5BCnAi1pIFsBT9eDFA7uUNxufZIQxapMTLdyIu6C1aFQ==' });

keystone.get('wysiwyg images',true);
keystone.get('wysiwyg cloudinary images',true);
keystone.get('wysiwyg additional buttons','styleselect');

require('./models');

keystone.set('routes', require('./routes'));

keystone.set('locals', {
	_: require('underscore'),
	js: 'javascript:;',
	env: keystone.get('env'),
	utils: keystone.utils,
	plural: keystone.utils.plural,
	editable: keystone.content.editable,
	google_api_key: keystone.get('google api key'),
});


keystone.set('nav', {
	'posts': ['posts', 'post-categories'],
	'users': 'users'
});

keystone.start(function(){
	//console.log(this);
});




